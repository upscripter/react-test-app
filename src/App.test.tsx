import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders company info', () => {
  const { getByText } = render(<App />);
  const description = getByText(/Microsoft Corporation/i);
  expect(description).toBeInTheDocument();
});
