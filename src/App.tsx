import React from 'react';
import Container from 'react-bootstrap/Container';
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom';
import { Navbar, Company } from './components';

const App = () => (
  <Router>
    <Container>
      <Navbar />
      <Switch>
        <Redirect exact from='/' to='/company/Microsoft' />
        <Route path='/company/:name' component={Company} />
      </Switch>
    </Container>
  </Router>
);

export default App;
