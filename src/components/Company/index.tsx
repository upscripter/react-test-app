import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import companies from 'data/companies.json';
import CompanyNews from './CompanyNews';

interface MatchParams {
  name: string;
}

const Heading = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;
`;

const Logo = styled.img`
  max-height: 60px;
  margin: 0 1rem;
`;

const Company: React.FC<RouteComponentProps<MatchParams>> = ({ match }) => {
  const { name } = match.params;
  const company = companies.find((c) => c.name === name) || null;
  return (
    company && (
      <>
        <Heading>
          <Logo src={company.logo} alt={`${company.name} logo`} />
          <h1>{company.name}</h1>
        </Heading>
        <main>{company.description}</main>
        <CompanyNews query={company.name} />
      </>
    )
  );
};

export default Company;
