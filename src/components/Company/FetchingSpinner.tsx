import React from 'react';
import styled from 'styled-components';
import Spinner from 'react-bootstrap/Spinner';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const FetchingSpinner: React.FC = () => (
  <Container>
    <Spinner animation='border' variant='primary' role='status' className='mr-2' />
    Fetching news...
  </Container>
);

export default FetchingSpinner;
