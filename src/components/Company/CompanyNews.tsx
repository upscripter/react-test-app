import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ListGroup from 'react-bootstrap/ListGroup';

import { newsService } from 'services';
import { IArticle } from 'types';
import FetchingSpinner from './FetchingSpinner';

const Container = styled.div`
  margin: 1rem 0;
`;

interface Props {
  query: string;
}

const CompanyNews: React.FC<Props> = ({ query }) => {
  const [fetching, setFetching] = useState(true);
  const [news, setNews] = useState<IArticle[]>([]);

  useEffect(() => {
    setFetching(true);
    newsService
      .getByQuery(query)
      .then(setNews)
      .finally(() => setFetching(false));
  }, [query]);

  return (
    <Container>
      {fetching ? (
        <FetchingSpinner />
      ) : (
        <ListGroup>
          {news.map((item) => (
            <ListGroup.Item action href={item.url} key={item.url} target='_blank'>
              {item.title}
            </ListGroup.Item>
          ))}
        </ListGroup>
      )}
    </Container>
  );
};

export default CompanyNews;
