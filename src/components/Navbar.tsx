import React from 'react';
import { NavLink } from 'react-router-dom';
import NavbarComponent from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import companies from 'data/companies.json';

const Navbar: React.FC = () => (
  <NavbarComponent bg='light'>
    <Nav variant='pills' navbar={false}>
      {companies.map(({ name }) => (
        <Nav.Item key={name}>
          <NavLink to={name} className='nav-link'>
            {name}
          </NavLink>
        </Nav.Item>
      ))}
    </Nav>
  </NavbarComponent>
);

export default Navbar;
