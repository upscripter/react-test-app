import { IArticle } from '../types';

const apiKey = process.env.REACT_APP_NEWS_API_KEY;

export const getByQuery = (query: string): Promise<IArticle[]> =>
  fetch(`https://newsapi.org/v2/everything?q=${query}&pageSize=5&apiKey=${apiKey}`)
    .then((res) => res.json())
    .then((data) => data?.articles || [])
    .catch(console.error);
